﻿using System;

namespace projectNotification
{
    class Program
    {
        static void Main(string[] args)
        {
            var userWithoutEmailAndPassword = new User(null, null);
            var userWithoutPassword = new User("user@gmail.com", null);
            var userWithoutEmail = new User(null, "P@ssw0rd");
            var user = new User("user@gmail.com", "P@ssw0rd");

            if(!userWithoutEmailAndPassword.IsValid())
            {
                Console.WriteLine($"userWithoutEmailAndPassword = {string.Join(";", userWithoutEmailAndPassword.Notifications)} ");
            }

            if(!userWithoutPassword.IsValid())
            {
                Console.WriteLine($"userWithoutPassword = {string.Join(";", userWithoutPassword.Notifications)}");
            }
            
            if(!userWithoutEmail.IsValid())
            {
                Console.WriteLine($"userWithoutEmail = {string.Join(";", userWithoutEmail.Notifications)}");
            }

            if(user.IsValid())
            {
                Console.WriteLine($"userWithoutEmail is valid.");
            }
            
            Console.ReadLine();
        }
    }
}
